package com.bancarelvalentin.personalarchivebot.process.scheduled.reminder


import com.bancarelvalentin.ezbot.process.scheduled.ScheduledProcess
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import com.bancarelvalentin.personalarchivebot.process.background.taskhandler.ReminderLogic
import com.bancarelvalentin.personalarchivebot.process.background.wishhandler.WishesLogic

class ReminderBot : ScheduledProcess() {

    override val rawName = "Reminder"
    override val rawDesc = "Rappel toute les tâches et wishes en cours"

    override val cron = "0 20 16 * * ? *"

    override fun logic() {
        ReminderLogic(gateway, this).remindMeta()
        WishesLogic(gateway, this).wishMeta()
        val checkMetaChannel = gateway.getTextChannelById(HardCodedValues.CHANNEL_CHECK_META)!!
        val messages = checkMetaChannel.history.retrievePast(1).complete()
        if (messages.isNotEmpty() && messages.any { it.contentRaw.isNotEmpty() }) {
            checkMetaChannel.sendMessage(FormatingUtils.formatUserId(HardCodedValues.USER__VBA)).complete()
        }
    }
}
