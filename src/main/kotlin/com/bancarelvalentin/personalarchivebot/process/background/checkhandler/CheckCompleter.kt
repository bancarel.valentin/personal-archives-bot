package com.bancarelvalentin.personalarchivebot.process.background.checkhandler

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class CheckCompleter(process: Process) : EventListener(process) {

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        super.onMessageReactionAdd(event)
        val channel = gateway.getTextChannelById(event.channel.id) ?: return
        if (channel.idLong == HardCodedValues.CHANNEL_CHECK_META
            && event.reaction.reactionEmote.emoji == "❓"
            && event.reaction.retrieveUsers().complete().map { it.idLong }.contains(HardCodedValues.USER__VBA)
        ) {
            val message = event.retrieveMessage().complete()
            message.clearReactions().complete()
            message.addReaction(EmojiEnum.OK_HAND.tounicode()).complete()
        }
    }

}