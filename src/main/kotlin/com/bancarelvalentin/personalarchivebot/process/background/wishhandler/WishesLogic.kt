package com.bancarelvalentin.personalarchivebot.process.background.wishhandler

import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.process.addreaction.AddReactionParams
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.personalarchivebot.BufferableTasks
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.exceptions.ErrorResponseException
import java.time.Duration
import java.util.stream.Collectors

class WishesLogic(val gateway: JDA, val process: Process) {

    var taskMap = gateway.textChannels.stream()
        .filter { it.idLong != HardCodedValues.CHANNEL_WISH_META && it.parentCategory?.idLong == HardCodedValues.CATEGORY_WISH_LISTS }
        .collect(Collectors.toMap(TextChannel::getName) { extractTasks(it) })

    var emojies =
        FormatingUtils.getRandomEmojies(taskMap.entries.stream().collect(Collectors.summingInt { it.value.size }))

    fun wishMeta() {
        AsyncUtils.buffer(
            BufferableTasks.WISHES_LOGIC.name,
            Duration.ofSeconds(30),
            Runnable {
                val channel = gateway.getTextChannelById(HardCodedValues.CHANNEL_WISH_META)!!
                ChannelUtils.emptyChannel(channel)
                val newMessage = channel.sendMessageEmbeds(wishesEmbedBuilder.build()).complete()
                addReactions(newMessage)
            })
    }

    val wishesEmbedBuilder: EmbedBuilder
        get() {
            val embed = EmbedBuilder().setTitle("Rappel de vos souhaits")

            var i = 0
            for ((channelName, messages) in taskMap) {
                if (messages.isEmpty()) continue
                var strTasks = ""
                for (message in messages) {
                    strTasks += "${emojies[i].tounicode()}\t-\t${message.contentRaw}\n"
                    i++
                }
                if (strTasks.length > 1024) {
                    strTasks = strTasks.substring(0, 1024)
                }
                embed.addField(channelName.replace("-", " ").capitalize(), strTasks, false)
            }
            return embed
        }

    private fun addReactions(message: Message) {
        var i = 0
        outer@ for (messages in taskMap.values) {
            for (task in messages) {
                if (i >= 20) {
                    process.logger.warn("Cannot add more wishes reactions")
                    break@outer
                }
                val emoji = emojies[i]
                i++
                try {
                    message.addReaction(emoji.tounicode()).queue()
                    process.addReactionEvent(
                        message,
                        emoji,
                        TaskCompleteReactionParam(task)
                    ) { _, param, event ->
                        WishCompleter.completeTask(event, (param as TaskCompleteReactionParam).message)
                    }
                } catch (e: ErrorResponseException) {
                    process.logger.error("error while adding a reaction for a wish to the meta list", throwable = e)
                    process.logger.debug("list: msg#${task.idLong} - ${task.contentRaw}")
                    process.logger.debug("wish: msg#${task.idLong} - ${task.contentRaw}")
                }
            }
        }
    }

    inner class TaskCompleteReactionParam(val message: Message) : AddReactionParams()

    private fun extractTasks(channel: TextChannel): List<Message> {
        var count = 0
        var new: Boolean
        val history = channel.history
        do {
            history.retrievePast(100).complete()
            new = history.retrievedHistory.size != count
            count = history.retrievedHistory.size
        } while (new)


        return history.retrievedHistory.stream()
            .filter { message ->
                message.reactions.stream()
                    .noneMatch { it.reactionEmote.isEmoji && it.reactionEmote.asReactionCode == "\uD83D\uDC4C" }
            }
            .collect(Collectors.toList())
    }

}