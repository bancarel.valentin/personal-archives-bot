package com.bancarelvalentin.personalarchivebot.process.background.safeplace

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent

class SafePlaceHandleReactionRemove(process: Process) : EventListener(process) {

    override fun onMessageReactionRemove(event: MessageReactionRemoveEvent) {
        super.onMessageReactionRemove(event)
        if (event.channel.idLong != HardCodedValues.CHANNEL_SAFE_PALCE) return
        val message = event.retrieveMessage().complete()
        if (message.reactions.isNotEmpty()) return
        message.delete().complete()
    }

}