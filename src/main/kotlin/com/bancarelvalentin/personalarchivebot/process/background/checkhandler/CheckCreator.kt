package com.bancarelvalentin.personalarchivebot.process.background.checkhandler

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class CheckCreator(process: Process) : EventListener(process) {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.author.isBot) return
        val channel = gateway.getTextChannelById(event.channel.id) ?: return
        if (channel.idLong != HardCodedValues.CHANNEL_CHECK_META) return
        event.message.addReaction(EmojiEnum.RED_QUESTION_MARK.tounicode()).complete()
    }
}