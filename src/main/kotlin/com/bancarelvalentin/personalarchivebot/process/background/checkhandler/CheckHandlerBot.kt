package com.bancarelvalentin.personalarchivebot.process.background.checkhandler


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

class CheckHandlerBot : BackgroundProcess() {

    override val rawName = "Check handler"
    override fun getListeners(): Array<EventListener> {
        return arrayOf(CheckCompleter(this), CheckCreator(this))
    }

    override val rawDesc = "Réagi avec :thumbs_up: si un user valide un check"


}
