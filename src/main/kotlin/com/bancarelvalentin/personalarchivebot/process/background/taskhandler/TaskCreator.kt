package com.bancarelvalentin.personalarchivebot.process.background.taskhandler

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.EmojiEnum
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.events.message.MessageReceivedEvent

class TaskCreator(process: Process) : EventListener(process) {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.author.isBot) return
        val channel = gateway.getTextChannelById(event.channel.id) ?: return
        if ((channel.parentCategory?.idLong ?: 0) != HardCodedValues.CATEGORY_TODO_LISTS) return
        if (channel.idLong == HardCodedValues.CHANNEL_TODO_META) return
        create(event.message)
        ReminderLogic(gateway, process).remindMeta()
    }

    companion object {
        fun create(message: Message) {
            message.addReaction(EmojiEnum.RED_QUESTION_MARK.tounicode()).complete()
        }
    }
}