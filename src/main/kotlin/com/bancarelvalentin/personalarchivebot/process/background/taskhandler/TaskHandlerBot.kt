package com.bancarelvalentin.personalarchivebot.process.background.taskhandler


import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess

class TaskHandlerBot : BackgroundProcess() {

    override val rawName = "Task handler"
    override fun getListeners(): Array<EventListener> {
        return arrayOf(TaskCreator(this), TaskCompleter(this), TaskReseter(this))
    }

    override val rawDesc =
        "Réagi aux task avec ? sur les nouvelles tâches; réagi avec :thumbs_up: si un user valide la task"


}
