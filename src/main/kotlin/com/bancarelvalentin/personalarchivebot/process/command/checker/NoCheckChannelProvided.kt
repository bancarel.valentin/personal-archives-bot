package com.bancarelvalentin.personalarchivebot.process.command.checker

import com.bancarelvalentin.ezbot.exception.usererrors.AbstractUserException

class NoCheckChannelProvided :
    AbstractUserException("Aucun channel fournit. Exécuter cette comande depuis un channel approprié ou fournissez les channels voulu en paramètres") {
}