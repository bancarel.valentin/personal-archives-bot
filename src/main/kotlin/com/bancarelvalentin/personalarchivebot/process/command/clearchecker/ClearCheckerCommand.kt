package com.bancarelvalentin.personalarchivebot.process.command.clearchecker

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.ezbot.utils.ChannelUtils
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.personalarchivebot.HardCodedValues
import java.util.function.BiConsumer

class ClearCheckerCommand : Command() {

    override val patterns = arrayOf("clear_checker", "clear", "cc")

    override val rawName = "Clear checker"
    override val rawDesc = "Vide le channel ${FormatingUtils.formatChannelId(HardCodedValues.CHANNEL_CHECK_META)}"

    override val logic = BiConsumer { _: CommandRequest, _: CommandResponse ->
        ChannelUtils.emptyChannel(gateway.getTextChannelById(HardCodedValues.CHANNEL_CHECK_META)!!)
    }
}
