package com.bancarelvalentin.personalarchivebot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.Config
import com.bancarelvalentin.personalarchivebot.process.background.taskhandler.ReminderLogic
import com.bancarelvalentin.personalarchivebot.process.background.taskhandler.TaskHandlerBot
import com.bancarelvalentin.personalarchivebot.process.background.wishhandler.WishHandlerBot
import com.bancarelvalentin.personalarchivebot.process.background.wishhandler.WishesLogic
import net.dv8tion.jda.api.JDA
import java.util.*


class PersonalArchiveConfig : Config {

    // Main
    override val versionNumber = Version().toString()

    override val prefixes = arrayOf(".", "/", "!")
    override val language: Locale = Locale.ENGLISH
    override val extraI18nProjectTokens = arrayOf("4aca5567ebcc46d38c7b15c0d11ada49")

    // Specific channels
    override val errorLogsChannelId = 931230547450167306
    override val lazyCommandsChannelId = 950486846457122857
    override val djDedicatedChannelId = 957611636296912896

    // Feature flags
    override val supportLazyCommands = true
    override val supportDjPlayerViaCommands = true
    override val supportDjDedicatedChannel = true
    override val logErrorsOnSentry = true
    override val timeStartupInSentry = true
    override val timeCommandExecutionsInSentry = true
    override val timeCronExecutionsInSentry = true

    // Access rights
    override val moderatorsIds = arrayOf(HardCodedValues.USER__VBA)
    override val defaultWhitelistedGuildIds = arrayOf(HardCodedValues.GUILD__PERSONAL_ARCHIVES)
    override val defaultWhitelistedChannelIds = arrayOf(HardCodedValues.CHANNEL_CMD)

    override val extraOnReadyListeners: Array<(JDA) -> Unit> = arrayOf(
        { it: JDA -> ReminderLogic(it, Orchestrator.getProcess(TaskHandlerBot::class.java)).remindMeta() },
        { WishesLogic(it, Orchestrator.getProcess(WishHandlerBot::class.java)).wishMeta() }
    )
}
