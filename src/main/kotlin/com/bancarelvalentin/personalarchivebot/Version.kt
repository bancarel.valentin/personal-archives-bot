package com.bancarelvalentin.personalarchivebot

import com.bancarelvalentin.ezbot.logger.Logger
import org.apache.commons.io.IOUtils
import java.nio.charset.Charset


class Version {

    private val version = try {
        val resourceAsStream = this.javaClass.getResourceAsStream("/version.txt")
        IOUtils.readLines(resourceAsStream, Charset.defaultCharset()).joinToString { it as String }
    } catch (e: Exception) {
        Logger(this.javaClass).error("Can't get version", throwable = e)
        "[ERROR]"
    }

    override fun toString(): String {
        return version
    }
}