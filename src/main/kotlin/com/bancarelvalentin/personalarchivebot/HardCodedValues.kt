package com.bancarelvalentin.personalarchivebot

@Suppress("MemberVisibilityCanBePrivate", "unused")
object HardCodedValues {

    // Values are for the "sndbox" guild if env mode is on; for the "GLRC" one otherwise

    // GUILDS
    val GUILD__PERSONAL_ARCHIVES = 920089643037368412


    //Categories
    val CATEGORY_TODO_LISTS: Long = 924775088342048798
    val CATEGORY_CHECK_LISTS: Long = 937733483550351390
    val CATEGORY_WISH_LISTS: Long = 937732722095427597

    // Channels
    val CHANNEL_CMD: Long = 948617439015493723
    val CHANNEL_TODO_META: Long = 925008021044723732
    val CHANNEL_CHECK_META: Long = 937733695870234654
    val CHANNEL_WISH_META: Long = 937734168589266944
    val CHANNEL_SAFE_PALCE: Long = 946850469219799070

    //Users
    const val USER__VBA: Long = 443750643375800320

    const val UNICODE_OK = "\u2705"
    const val UNICODE_WARN = "\u2757"
    const val UNICODE_KO = "\u274C"
}
