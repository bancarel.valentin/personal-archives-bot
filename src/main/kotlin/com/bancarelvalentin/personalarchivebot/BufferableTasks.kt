package com.bancarelvalentin.personalarchivebot

enum class BufferableTasks {
    REMINDER_LOGIC,
    WISHES_LOGIC,
}